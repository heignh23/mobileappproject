import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.mobilecomputingapp.Party
import com.example.mobilecomputingapp.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class CustomInfoWindowAdapter(private val context: Context) : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker): View? {
        // Retourne null pour utiliser la vue par défaut
        return null
    }

    override fun getInfoContents(marker: Marker): View {
        // Personnalisez la vue de l'info window ici
        val contentView = LayoutInflater.from(context).inflate(R.layout.custom_info_window, null)

        val titleTextView: TextView = contentView.findViewById(R.id.titleTextView)
        val snippetTextView: TextView = contentView.findViewById(R.id.snippetTextView)

        // Obtenez les données de la fête à partir du tag du marqueur
        val party = marker.tag as Party

        // Mettez à jour la vue avec les données de la fête
        titleTextView.text = party.name
        snippetTextView.text = party.address

        return contentView
    }
}
