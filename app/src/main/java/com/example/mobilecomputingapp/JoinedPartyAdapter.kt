import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilecomputingapp.MainActivity
import com.example.mobilecomputingapp.JoinedActivity
import com.example.mobilecomputingapp.Party
import com.example.mobilecomputingapp.R

class JoinedPartyAdapter(private val context: Context, private var joinedpartyList: List<Party>) :
    RecyclerView.Adapter<JoinedPartyAdapter.PartyViewHolder>() {

    // ViewHolder
    inner class PartyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val partyName: TextView = itemView.findViewById(R.id.partyName)
        val partyAddress: TextView = itemView.findViewById(R.id.partyAddress)
        val partydate: TextView = itemView.findViewById(R.id.partyDate)
        val partyinfo: Button = itemView.findViewById(R.id.joinedpartyinfo)
        val partyleave: Button = itemView.findViewById(R.id.joinedLeave)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PartyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.joined_party_item, parent, false)
        return PartyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PartyViewHolder, position: Int) {
        val currentParty = joinedpartyList[position]

        // Mettez à jour les vues avec les données de la fête actuelle
        holder.partyName.text = currentParty.name
        holder.partyAddress.text = currentParty.address
        holder.partydate.text = currentParty.date

        // Ajoutez un écouteur de clic au bouton de jointure si nécessaire
        holder.partyinfo.setOnClickListener {
            when (context) {
                is JoinedActivity -> {
                    val joinedActivity = context as JoinedActivity
                    joinedActivity.showJoinedPartyInfoDialog(currentParty)
                }
                // Add more cases for other activity contexts if needed
                else -> {
                    Toast.makeText(context, "problème d'annonce chez joined party adapter", Toast.LENGTH_SHORT).show()
                }
            }
        }
        holder.partyleave.setOnClickListener {
            when (context) {
                is JoinedActivity -> {
                    val joinedActivity = context as JoinedActivity
                    joinedActivity.onLeaveButtonClicked(currentParty)
                }
                // Add more cases for other activity contexts if needed
                else -> {
                    Toast.makeText(context, "problème d'annonce chez joined party adapter", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return joinedpartyList.size
    }

    // Fonction pour mettre à jour les données de l'adaptateur
    fun updateData(newPartyList: List<Party>) {
        joinedpartyList = newPartyList
        notifyDataSetChanged()
    }
}
