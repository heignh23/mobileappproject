import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilecomputingapp.JoinedActivity
import com.example.mobilecomputingapp.MainActivity
import com.example.mobilecomputingapp.Party
import com.example.mobilecomputingapp.R

class PartyAdapter(private val context: Context, private var partyList: List<Party>) :
    RecyclerView.Adapter<PartyAdapter.PartyViewHolder>() {

    // ViewHolder
    inner class PartyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val partyName: TextView = itemView.findViewById(R.id.partyName)
        val partyAddress: TextView = itemView.findViewById(R.id.partyAddress)
        val partydate: TextView = itemView.findViewById(R.id.partyDate)
        val partyinfo: Button = itemView.findViewById(R.id.partyinfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PartyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.party_item, parent, false)
        return PartyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PartyViewHolder, position: Int) {
        val currentParty = partyList[position]

        // Mettez à jour les vues avec les données de la fête actuelle
        holder.partyName.text = currentParty.name
        holder.partyAddress.text = currentParty.address
        holder.partydate.text = currentParty.date

        // Ajoutez un écouteur de clic au bouton de jointure si nécessaire
        holder.partyinfo.setOnClickListener {
            when (context) {
                is MainActivity -> {
                    val mainActivity = context as MainActivity
                    mainActivity.showPartyInfoDialog(currentParty)
                }
                // Add more cases for other activity contexts if needed
                else -> {
                    // Handle the case where the context is not MainActivity or JoinedActivity
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return partyList.size
    }

    // Fonction pour mettre à jour les données de l'adaptateur
    fun updateData(newPartyList: List<Party>) {
        partyList = newPartyList
        notifyDataSetChanged()
    }
}

