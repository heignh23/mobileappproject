package com.example.mobilecomputingapp

import java.io.Serializable

data class Party (
    val id: String? = null,
    var name: String? = "",
    var address: String? = "",
    var date: String? = "",
    var from: String? = "",
    var to: String? = "",
    var description: String? = "",
    var latitude: Double? = 0.0,
    var longitude: Double? = 0.0,
    var partyOwner: String? = "",
    var numberMax: Int? = 0,
    var price: Double? = 0.00,
    var paymentLink: String? = "",
    var number: Int? = 0,
    var applied: List<String> = listOf()
) : Serializable
