package com.example.mobilecomputingapp

import MyPartyAdapter
import JoinedPartyAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilecomputingapp.R.id
import com.example.mobilecomputingapp.R.id.buttonClose
import com.example.mobilecomputingapp.databinding.ActivityJoinedBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class JoinedActivity : AppCompatActivity() {

    private lateinit var binding: ActivityJoinedBinding
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var databaseReferenceParties: DatabaseReference
    private lateinit var joinedPartyAdapter: JoinedPartyAdapter
    private lateinit var myPartiesAdapter: MyPartyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJoinedBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val toolbar: Toolbar = findViewById(id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        firebaseAuth = FirebaseAuth.getInstance()
        databaseReferenceParties = FirebaseDatabase.getInstance().reference.child("parties")

        joinedPartyAdapter = JoinedPartyAdapter(this, emptyList())
        myPartiesAdapter = MyPartyAdapter(this, emptyList())


        val joinedRecyclerView: RecyclerView = findViewById(id.joinedRecyclerView)
        joinedRecyclerView.layoutManager = LinearLayoutManager(this)
        joinedRecyclerView.adapter = joinedPartyAdapter  // Utilisez joinedPartyAdapter ici

        val mypartiesRecyclerView: RecyclerView = findViewById(id.mypartiesRecyclerView)
        mypartiesRecyclerView.layoutManager = LinearLayoutManager(this)
        mypartiesRecyclerView.adapter = myPartiesAdapter



        databaseReferenceParties.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                // Le code ici sera appelé lorsqu'un nouvel enfant est ajouté à la table
                val valeur = snapshot.child("parties").getValue(String::class.java)
                // Faites quelque chose avec la valeur...
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                // Le code ici sera appelé lorsqu'un enfant existant est modifié
                val valeur = snapshot.child("parties").getValue(String::class.java)
                // Faites quelque chose avec la nouvelle valeur...
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                // Le code ici sera appelé lorsqu'un enfant est supprimé de la table
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                // Le code ici sera appelé lorsqu'un enfant change de position dans la table
            }

            override fun onCancelled(error: DatabaseError) {
                // Gérer les erreurs ici
            }
        })

        val userEmail = getUserEmail()
        Log.d("email", userEmail.toString())
        if (userEmail != null) {
            // Afficher les parties auxquelles l'utilisateur actuel a adhéré
            retrieveJoinedPartiesFromDatabase(userEmail)

            // Afficher les parties dont l'utilisateur actuel est le propriétaire
            retrieveMyPartiesFromDatabase(userEmail)
        }

        binding.backButton.setOnClickListener{
            onBackPressed()
        }
    }


    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun retrieveJoinedPartiesFromDatabase(userEmail: String) {
        databaseReferenceParties.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val joinedParties = mutableListOf<Party>()

                // Parcourir les données de la base de données
                for (partySnapshot in snapshot.children) {
                    val party = partySnapshot.getValue(Party::class.java)

                    // Vérifier si l'utilisateur actuel a adhéré à la fête
                    if (party?.applied?.contains(userEmail) == true) {
                        joinedParties.add(party)
                    }
                }

                // Mettre à jour l'adaptateur de parties rejointes
                joinedPartyAdapter.updateData(joinedParties)
            }

            override fun onCancelled(error: DatabaseError) {
                // Gérer l'erreur, si nécessaire
            }
        })
    }

    private fun retrieveMyPartiesFromDatabase(userEmail: String) {
        databaseReferenceParties.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val myParties = mutableListOf<Party>()

                // Parcourir les données de la base de données
                for (partySnapshot in snapshot.children) {
                    val party = partySnapshot.getValue(Party::class.java)

                    // Vérifier si l'utilisateur actuel est le propriétaire de la fête
                    if (party?.partyOwner == userEmail) {
                        myParties.add(party)
                    }
                }

                // Mettre à jour l'adaptateur de mes parties
                myPartiesAdapter.updateData(myParties)
            }

            override fun onCancelled(error: DatabaseError) {
                // Gérer l'erreur, si nécessaire
            }
        })
    }

    fun showJoinedPartyInfoDialog(party: Party) {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.joined_party_info_dialog, null)

        // Récupérez les vues à partir de la mise en page XML
        val textViewPartyName: TextView = dialogView.findViewById(id.textViewPartyName)
        val textViewPartyOwner: TextView = dialogView.findViewById(id.textViewPartyOwner)
        val textViewPartyAddress: TextView = dialogView.findViewById(id.textViewPartyAddress)
        val textViewPartyDate: TextView = dialogView.findViewById(id.textViewPartyDate)
        val textViewPartyFrom: TextView = dialogView.findViewById(id.textViewPartyFrom)
        val textViewPartyTo: TextView = dialogView.findViewById(id.textViewPartyTo)
        val textViewPartyDescription: TextView = dialogView.findViewById(id.textViewPartyDescription)
        val textViewPartyPrice: TextView = dialogView.findViewById(id.textViewPartyPrice)
        val textViewPartyQty: TextView = dialogView.findViewById(id.textViewPartyquantity)


        val buttonLeave: Button = dialogView.findViewById(id.buttonLeave)
        val buttonClose: Button = dialogView.findViewById(buttonClose)

        // Configurez les valeurs des vues avec les informations de la fête
        textViewPartyName.text = "${party.name}"
        textViewPartyOwner.text = "Host's Email: ${party.partyOwner}"
        textViewPartyAddress.text = "Event Address: ${party.address}"
        textViewPartyDate.text = "Date: ${party.date}"
        textViewPartyFrom.text = "From: ${party.from}"
        textViewPartyTo.text = "To: ${party.to}"
        textViewPartyPrice.text = "Price: ${party.price}"
        textViewPartyQty.text = "Available ticket: ${party.numberMax?.minus((party.number ?: 0))}"

        textViewPartyDescription.text = "Description:\n ${party.description}"

        // Ajoutez d'autres lignes pour configurer les autres TextView avec les informations de la fête

        // Créez le dialogue
        val builder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setCancelable(true)

        val dialog = builder.create()

        // Configurez les actions des boutons Join et Close

        buttonLeave.setOnClickListener {
            onLeaveButtonClicked(party)
            dialog.dismiss()
        }

        buttonClose.setOnClickListener {
            dialog.dismiss()
        }

        // Affichez la fenêtre contextuelle
        dialog.show()
    }

    fun showMyPartyInfoDialog(party: Party) {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.my_party_info_dialog, null)

        // Récupérez les vues à partir de la mise en page XML
        val textViewPartyName: TextView = dialogView.findViewById(R.id.textViewPartyName)
        val textViewPartyOwner: TextView = dialogView.findViewById(R.id.textViewPartyOwner)
        val textViewPartyAddress: TextView = dialogView.findViewById(R.id.textViewPartyAddress)
        val textViewPartyDate: TextView = dialogView.findViewById(R.id.textViewPartyDate)
        val textViewPartyFrom: TextView = dialogView.findViewById(R.id.textViewPartyFrom)
        val textViewPartyTo: TextView = dialogView.findViewById(R.id.textViewPartyTo)
        val textViewPartyDescription: TextView = dialogView.findViewById(R.id.textViewPartyDescription)
        val textViewPartyPrice: TextView = dialogView.findViewById(R.id.textViewPartyPrice)
        val textViewPartyQty: TextView = dialogView.findViewById(R.id.textViewPartyquantity)
        val textViewApplied: TextView = dialogView.findViewById(R.id.textViewApplied)

        val buttonModify: Button = dialogView.findViewById(R.id.buttonModify)
        val buttonDelete: Button = dialogView.findViewById(R.id.buttonDelete)
        val buttonClose: Button = dialogView.findViewById(R.id.buttonClose)

        // Configurez les valeurs des vues avec les informations de la fête
        textViewPartyName.text = "${party.name}"
        textViewPartyOwner.text = "Host's Email: ${party.partyOwner}"
        textViewPartyAddress.text = "Event Address: ${party.address}"
        textViewPartyDate.text = "Date: ${party.date}"
        textViewPartyFrom.text = "From: ${party.from}"
        textViewPartyTo.text = "To: ${party.to}"
        textViewPartyPrice.text = "Price: ${party.price}"
        textViewPartyQty.text = "Available ticket: ${party.numberMax?.minus((party.number ?: 0))}"

        textViewPartyDescription.text = "Description:\n ${party.description}"

        // Configurez le texte pour les personnes qui ont postulé
        val appliedText = "People who applied:\n${party.applied.joinToString("\n")}"
        textViewApplied.text = appliedText

        // Ajoutez d'autres lignes pour configurer les autres TextView avec les informations de la fête

        // Créez le dialogue
        val builder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setCancelable(true)

        val dialog = builder.create()

        // Configurez les actions des boutons Modify, Delete, et Close

        buttonModify.setOnClickListener {
            // Ouvrez le formulaire de modification et passez les informations de la fête
            val intent = Intent(this, ModifyPartyForm::class.java)
            intent.putExtra("partyToModify", party)
            startActivity(intent)
            dialog.dismiss()
        }

        buttonDelete.setOnClickListener {
            // Supprimez la fête de la base de données en utilisant son ID
            party.id?.let { partyId ->
                val databaseReferenceParties = FirebaseDatabase.getInstance().reference.child("parties")
                databaseReferenceParties.child(partyId).removeValue()
                    .addOnSuccessListener {
                        // Mettez à jour l'adaptateur après la suppression
                        myPartiesAdapter.removeParty(party)
                        Toast.makeText(this, "Event deleted successfully", Toast.LENGTH_SHORT).show()
                        dialog.dismiss()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "Failed to delete Event", Toast.LENGTH_SHORT).show()
                        dialog.dismiss()
                    }
            }
        }

        buttonClose.setOnClickListener {
            dialog.dismiss()
        }

        // Affichez la fenêtre contextuelle
        dialog.show()
    }


    private fun getUserEmail(): String? {
        val sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        // Obtenez l'adresse e-mail enregistrée, si elle existe
        return sharedPreferences.getString("userEmail", null)
    }

    fun onLeaveButtonClicked(party: Party) {
        val userEmail = getUserEmail()

        if (party.applied.contains(userEmail)) {
            // Supprimez l'utilisateur actuel de la liste des participants
            val updatedAppliedList = party.applied.toMutableList().apply {
                remove(userEmail)
            }

            // Mettez à jour la base de données avec la nouvelle liste
            databaseReferenceParties.child(party.id.orEmpty()).child("applied")
                .setValue(updatedAppliedList)

            // Mettez à jour le nombre de participants
            val currentNumber = party.number ?: 0
            databaseReferenceParties.child(party.id.orEmpty()).child("number")
                .setValue(currentNumber - 1)

            showToast("You have left the event!")
        } else {
            showToast("You have not joined this event.")
        }
    }
}