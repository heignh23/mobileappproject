package com.example.mobilecomputingapp

import CustomInfoWindowAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.mobilecomputingapp.databinding.ActivityMapsBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import android.content.Intent
import android.location.LocationManager
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.core.net.toUri
import com.google.android.gms.maps.model.Marker
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DatabaseReference


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    private lateinit var currentLocation: Location
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val permissionCode = 101
    private lateinit var databaseReferenceParties: DatabaseReference
    private val markers = mutableListOf<Marker>()

    private val partiesList = mutableListOf<Party>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        databaseReferenceParties = FirebaseDatabase.getInstance().reference.child("parties")

        if (markers.isEmpty()) {
            markers.clear()
        }

        databaseReferenceParties.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("Firebase", "onChildAdded")
                val party = snapshot.getValue(Party::class.java)
                party?.let {
                    // Ajoutez un marqueur sur la carte avec les données de la fête nouvellement ajoutée
                    addMarkerOnMap(it)
                    Log.d("Marker", "Marker added successfully")
                    Log.d("Marker", "Markers size: ${markers.size}")
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("Firebase", "onChildChanged")
                val updatedParty = snapshot.getValue(Party::class.java)
                updatedParty?.let {
                    Log.d("Firebase", "Updated Party ID: ${it.id}")
                    // Mettez à jour le marqueur sur la carte avec les nouvelles données de la fête modifiée
                    updateMarkerOnMap(it)

                    // Mettez à jour le tag du marqueur avec les nouvelles données de la fête
                    var markerUpdated = false
                    for (marker in markers) {
                        val party = marker.tag as? Party
                        if (party?.id == updatedParty.id) {
                            marker.tag = updatedParty
                            markerUpdated = true
                            Log.d("Marker", "Marker updated successfully")
                            break
                        }
                    }

                    if (!markerUpdated) {
                        Log.e("Marker", "Marker not found for party with ID: ${updatedParty?.id}")
                        Log.e("Marker", "Markers size: ${markers.size}")
                    }
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                Log.d("Firebase", "onChildRemoved")
                val removedParty = snapshot.getValue(Party::class.java)
                removedParty?.let {
                    // Supprimez le marqueur correspondant sur la carte lorsque la fête est supprimée
                    removeMarkerFromMap(it)
                }
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                // Peut être ignoré, sauf si vous avez besoin de gérer les mouvements des enfants
            }

            override fun onCancelled(error: DatabaseError) {
                // Gérer les erreurs ici
            }
        })

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val databaseReferenceParties = FirebaseDatabase.getInstance().getReference("parties")
        databaseReferenceParties.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                partiesList.clear()
                for (partySnapshot in dataSnapshot.children) {
                    val party = partySnapshot.getValue(Party::class.java)
                    party?.let { partiesList.add(it) }
                }
                // Ajoutez des marqueurs sur la carte avec les données récupérées
                Log.d("test","onchangedata")
                addMarkersOnMap()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Gérez l'erreur
            }
        })
        binding.listButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        binding.fab.setOnClickListener {
            startActivity(Intent(this, AddPartyForm::class.java))
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun placeMarker(latLng: LatLng, annotation: String) {
        // Coloca un marcador en la ubicación proporcionada
        mMap.addMarker(MarkerOptions().position(latLng).title(annotation))

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
    }

    private fun getCurrentLocationUser() {
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION
            ) !=
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), permissionCode)
            return
        }

        // Obtenez la dernière position connue de l'utilisateur
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                currentLocation = location
                val userLatLng = LatLng(currentLocation.latitude, currentLocation.longitude)

                // Zoom sur la position actuelle
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 15f))
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            permissionCode -> if (grantResults.isNotEmpty() && grantResults[0] ==
                PackageManager.PERMISSION_GRANTED
            ) {
                getCurrentLocationUser()
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        // Le map est prêt pour être utilisé
        mMap = googleMap

        // Initialiser la liste des marqueurs
        markers.clear()

        // Masquer les boutons d'itinéraire et de Maps
        mMap.uiSettings.isMapToolbarEnabled = false

        // Configurer l'adapter pour l'info window
        mMap.setInfoWindowAdapter(CustomInfoWindowAdapter(this))

        // Gérer le clic sur l'info window
        mMap.setOnInfoWindowClickListener { marker ->
            val party = marker.tag as? Party
            party?.let {
                // Afficher la fenêtre d'informations avec les boutons "Close" et "Join"
                showPartyInfoWindow(it)
            }

        }

        // Vérifier si la localisation est activée
        if (isLocationEnabled()) {
            // Localisation activée, zoom sur la position actuelle
            getCurrentLocationUser()
        } else {
            // Localisation désactivée, zoom sur la ville de Reutlingen
            zoomToReutlingen()
        }

        // Attribuer des tags aux marqueurs à partir des données de fête
        Log.d("test","onmapready")
        addMarkersOnMap()
    }

    private fun addMarkersOnMap() {
        if (::mMap.isInitialized) {
            for (party in partiesList) {
                val partyLocation = LatLng(party.latitude ?: 0.0, party.longitude ?: 0.0)
                val marker = mMap.addMarker(
                    MarkerOptions()
                        .position(partyLocation)
                        .title(party.name)
                )
                marker.tag = party // Attachez les données de la fête au marqueur
                markers.add(marker) // Ajoutez le marqueur à la liste
            }
        } else {
            // Attendre que mMap soit initialisé, par exemple, en plaçant une logique de réessai
            Handler().postDelayed({
                Log.d("test","addmarkersonmap")
                addMarkersOnMap()
            }, 100) // Attendre 100 millisecondes avant de réessayer
        }
    }

    @SuppressLint("CutPasteId")
    private fun showPartyInfoWindow(party: Party) {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.party_info_dialog, null)

        // Récupérez les vues à partir de la mise en page XML
        val textViewPartyName: TextView = dialogView.findViewById(R.id.textViewPartyName)
        val textViewPartyOwner: TextView = dialogView.findViewById(R.id.textViewPartyOwner)
        val textViewPartyAddress: TextView = dialogView.findViewById(R.id.textViewPartyAddress)
        val textViewPartyDate: TextView = dialogView.findViewById(R.id.textViewPartyDate)
        val textViewPartyFrom: TextView = dialogView.findViewById(R.id.textViewPartyFrom)
        val textViewPartyTo: TextView = dialogView.findViewById(R.id.textViewPartyTo)
        val textViewPartyDescription: TextView = dialogView.findViewById(R.id.textViewPartyDescription)
        val textViewPartyPrice: TextView = dialogView.findViewById(R.id.textViewPartyPrice)
        val textViewPartyQty: TextView = dialogView.findViewById(R.id.textViewPartyquantity)


        val buttonJoin: Button = dialogView.findViewById(R.id.buttonJoin)
        val buttonClose: Button = dialogView.findViewById(R.id.buttonClose)

        // Configurez les valeurs des vues avec les informations de la fête
        textViewPartyName.text = "${party.name}"
        textViewPartyOwner.text = "Host's Email: ${party.partyOwner}"
        textViewPartyAddress.text = "Event Address: ${party.address}"
        textViewPartyDate.text = "Date: ${party.date}"
        textViewPartyFrom.text = "From: ${party.from}"
        textViewPartyTo.text = "To: ${party.to}"
        textViewPartyPrice.text = "Price: ${party.price}"
        textViewPartyQty.text = "Available ticket: ${party.numberMax?.minus((party.number ?: 0))}"

        textViewPartyDescription.text = "Description:\n ${party.description}"

        // Ajoutez d'autres lignes pour configurer les autres TextView avec les informations de la fête

        // Créez le dialogue
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
            .setView(dialogView)
            .setCancelable(true)

        val dialog = builder.create()

        // Configurez les actions des boutons Join et Close
        val joinButton = dialogView.findViewById<Button>(R.id.buttonJoin)
        joinButton.setOnClickListener {
            onJoinButtonClicked(party)
            dialog.dismiss()
        }

        val closeButton = dialogView.findViewById<Button>(R.id.buttonClose)
        closeButton.setOnClickListener {
            dialog.dismiss()
        }

        // Affichez la fenêtre contextuelle
        dialog.show()
    }

    private fun onJoinButtonClicked(party: Party) {
        val userEmail = getUserEmail()

        if (party.applied.contains(userEmail)) {
            showToast("You have already joined this event.")
            return
        }

        val currentNumber = party.number ?: 0
        val maxNumber = party.numberMax ?: 0

        Log.d("JoinButtonClicked", "Current Number: $currentNumber, Max Number: $maxNumber")

        if (currentNumber < maxNumber) {
            if (party.paymentLink.isNullOrEmpty()) {
                // Entrée gratuite
                val updatedAppliedList = party.applied.toMutableList().apply {
                    userEmail?.let { add(it) }
                }

                // Mettre à jour la base de données avec la nouvelle liste
                databaseReferenceParties.child(party.id.orEmpty()).child("applied")
                    .setValue(updatedAppliedList)

                // Mettre à jour le nombre de participants
                databaseReferenceParties.child(party.id.orEmpty()).child("number")
                    .setValue(currentNumber + 1)

                showToast("You have joined the event!")

            } else {
                // Entrée payante
                if (party.paymentLink?.isNotEmpty() == true) {
                    val intent = Intent(Intent.ACTION_VIEW, party.paymentLink!!.toUri())
                    startActivity(intent)
                } else {
                    showToast("Payment link is empty.")
                }
            }
        } else {
            showToast("All tickets are already taken!")
        }
    }
    private fun getUserEmail(): String? {
        val sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        // Obtenez l'adresse e-mail enregistrée, si elle existe
        return sharedPreferences.getString("userEmail", null)
    }

    private fun addMarkerOnMap(party: Party) {
        // Vérifie si mMap est initialisé
        if (::mMap.isInitialized) {
            val partyLocation = LatLng(party.latitude ?: 0.0, party.longitude ?: 0.0)
            val marker = mMap.addMarker(
                MarkerOptions()
                    .position(partyLocation)
                    .title(party.name)
            )
            marker.tag = party // Attachez les données de la fête au marqueur
            markers.add(marker) // Ajoutez le marqueur à la liste
        } else {
            // Attendre que mMap soit initialisé, par exemple, en plaçant une logique de réessai
            Handler().postDelayed({
                Log.d("test","addmarkersonmap")
                addMarkersOnMap()
            }, 100) // Attendre 100 millisecondes avant de réessayer
        }
    }
    private fun updateMarkerOnMap(updatedParty: Party) {
        for (marker in markers) {
            val party = marker.tag as? Party
            if (party?.id == updatedParty.id) {
                marker.tag = updatedParty // Mettez à jour les données de la fête dans le tag du marqueur
                // Mettez à jour d'autres informations du marqueur si nécessaire
            }
        }
    }

    private fun removeMarkerFromMap(removedParty: Party) {
        val markerToRemove = markers.firstOrNull { marker ->
            val party = marker.tag as? Party
            party?.id == removedParty.id
        }

        markerToRemove?.remove()
        markers.remove(markerToRemove)
    }
    private fun clearMarkers() {
        for (marker in markers) {
            marker.remove()
        }
        markers.clear()
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    private fun zoomToReutlingen() {
        val reutlingen = LatLng(48.4919, 9.2046)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(reutlingen, 15f))
    }
}
