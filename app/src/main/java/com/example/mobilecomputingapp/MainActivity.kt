package com.example.mobilecomputingapp

import PartyAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilecomputingapp.R.id
import com.example.mobilecomputingapp.R.id.buttonClose
import com.example.mobilecomputingapp.databinding.ActivityMainBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var databaseReferenceParties: DatabaseReference
    private lateinit var partyAdapter: PartyAdapter
    private lateinit var fab: FloatingActionButton
    private lateinit var mapButton: FloatingActionButton
    private lateinit var mMap: GoogleMap
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val toolbar: Toolbar = findViewById(id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        firebaseAuth = FirebaseAuth.getInstance()
        databaseReferenceParties = FirebaseDatabase.getInstance().reference.child("parties")

        val recyclerView: RecyclerView = findViewById(id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        partyAdapter = PartyAdapter(this, emptyList())
        recyclerView.adapter = partyAdapter


        databaseReferenceParties.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                // Le code ici sera appelé lorsqu'un nouvel enfant est ajouté à la table
                val valeur = snapshot.child("parties").getValue(String::class.java)
                // Faites quelque chose avec la valeur...
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                // Le code ici sera appelé lorsqu'un enfant existant est modifié
                val valeur = snapshot.child("parties").getValue(String::class.java)
                // Faites quelque chose avec la nouvelle valeur...
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                // Le code ici sera appelé lorsqu'un enfant est supprimé de la table
                val valeur = snapshot.child("parties").getValue(String::class.java)
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                // Le code ici sera appelé lorsqu'un enfant change de position dans la table
            }

            override fun onCancelled(error: DatabaseError) {
                // Gérer les erreurs ici
            }
        })

        fab = findViewById(R.id.fab)
        fab.setOnClickListener {
            startActivity(Intent(this, AddPartyForm::class.java))
        }
        mapButton = findViewById(R.id.mapButton)
        mapButton.setOnClickListener {
            startActivity(Intent(this, MapsActivity::class.java))
        }

        binding.accountButton.setOnClickListener {
            startActivity(Intent(this, JoinedActivity::class.java))
        }


        retrievePartiesFromDatabase()

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.mainmenu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            id.signout -> {
                firebaseAuth.signOut()
                removeUserEmail()
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun removeUserEmail() {
        val sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        // Supprimez la préférence de l'adresse e-mail
        editor.remove("userEmail")
        editor.apply()
    }

    private fun retrievePartiesFromDatabase() {
        // Ajouter un écouteur pour surveiller les changements dans la base de données
        databaseReferenceParties.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val parties = mutableListOf<Party>()

                // Parcourir les données de la base de données
                for (partySnapshot in snapshot.children) {
                    val party = partySnapshot.getValue(Party::class.java)
                    party?.let { parties.add(it) }
                }

                // Filtrer les fêtes dont la date est inférieure à la date d'aujourd'hui
                val currentDate = SimpleDateFormat("dd/M/yyyy", Locale.getDefault()).format(Date())
                val filteredParties = parties.filter { party ->
                    val partyDate = party.date ?: ""
                    partyDate.isNotEmpty() && isAfterCurrentDate(partyDate, currentDate)
                }

                // Trier les fêtes restantes par date, du plus proche au plus éloigné
                val sortedPartyList = filteredParties.sortedBy { party ->
                    calculateDistance(
                        48.483334,
                        9.216667,
                        party.latitude ?: 0.0,
                        party.longitude ?: 0.0
                    )
                }

                // Mettre à jour l'adaptateur avec les nouvelles données triées
                partyAdapter.updateData(sortedPartyList)
            }

            override fun onCancelled(error: DatabaseError) {
                // Gérer l'erreur, si nécessaire
            }
        })
    }

    // Fonction pour vérifier si la date de la fête est après la date d'aujourd'hui
    private fun isAfterCurrentDate(partyDate: String, currentDate: String): Boolean {
        val sdf = SimpleDateFormat("dd/mm/yyyy", Locale.getDefault())
        val date1 = sdf.parse(partyDate)
        val date2 = sdf.parse(currentDate)

        return date1 != null && date2 != null && (date1.after(date2) || date1.equals(date2))
    }

    private fun calculateDistance(
        lat1: Double,
        lon1: Double,
        lat2: Double,
        lon2: Double
    ): Double {
        val R = 6371 // Rayon de la Terre en kilomètres
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lon2 - lon1)
        val a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) *
                    Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return R * c
    }

    fun showPartyInfoDialog(party: Party) {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.party_info_dialog, null)

        // Récupérez les vues à partir de la mise en page XML
        val textViewPartyName: TextView = dialogView.findViewById(id.textViewPartyName)
        val textViewPartyOwner: TextView = dialogView.findViewById(id.textViewPartyOwner)
        val textViewPartyAddress: TextView = dialogView.findViewById(id.textViewPartyAddress)
        val textViewPartyDate: TextView = dialogView.findViewById(id.textViewPartyDate)
        val textViewPartyFrom: TextView = dialogView.findViewById(id.textViewPartyFrom)
        val textViewPartyTo: TextView = dialogView.findViewById(id.textViewPartyTo)
        val textViewPartyDescription: TextView = dialogView.findViewById(id.textViewPartyDescription)
        val textViewPartyPrice: TextView = dialogView.findViewById(id.textViewPartyPrice)
        val textViewPartyQty: TextView = dialogView.findViewById(id.textViewPartyquantity)


        val buttonJoin: Button = dialogView.findViewById(id.buttonJoin)
        val buttonClose: Button = dialogView.findViewById(buttonClose)

        // Configurez les valeurs des vues avec les informations de la fête
        textViewPartyName.text = "${party.name}"
        textViewPartyOwner.text = "Host's Email: ${party.partyOwner}"
        textViewPartyAddress.text = "Event Address: ${party.address}"
        textViewPartyDate.text = "Date: ${party.date}"
        textViewPartyFrom.text = "From: ${party.from}"
        textViewPartyTo.text = "To: ${party.to}"
        textViewPartyPrice.text = "Price: ${party.price}"
        textViewPartyQty.text = "Available tickets: ${party.numberMax?.minus((party.number ?: 0))}"

        textViewPartyDescription.text = "Description:\n ${party.description}"

        // Ajoutez d'autres lignes pour configurer les autres TextView avec les informations de la fête

        // Créez le dialogue
        val builder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setCancelable(true)

        val dialog = builder.create()

        // Configurez les actions des boutons Join et Close
        val joinButton = dialogView.findViewById<Button>(id.buttonJoin)

        joinButton.setOnClickListener {
            onJoinButtonClicked(party)
            dialog.dismiss()
        }

        val closeButton = dialogView.findViewById<Button>(id.buttonClose)
        closeButton.setOnClickListener {
            dialog.dismiss()
        }

        // Affichez la fenêtre contextuelle
        dialog.show()
    }
    private fun onJoinButtonClicked(party: Party) {
        val userEmail = getUserEmail()

        if (party.applied.contains(userEmail)) {
            showToast("You have already joined this event.")
            return
        }

        val currentNumber = party.number ?: 0
        val maxNumber = party.numberMax ?: 0

        Log.d("JoinButtonClicked", "Current Number: $currentNumber, Max Number: $maxNumber")

        if (currentNumber < maxNumber) {
            if (party.paymentLink.isNullOrEmpty()) {
                // Entrée gratuite
                val updatedAppliedList = party.applied.toMutableList().apply {
                    userEmail?.let { add(it) }
                }

                // Mettre à jour la base de données avec la nouvelle liste
                databaseReferenceParties.child(party.id.orEmpty()).child("applied")
                    .setValue(updatedAppliedList)

                // Mettre à jour le nombre de participants
                databaseReferenceParties.child(party.id.orEmpty()).child("number")
                    .setValue(currentNumber + 1)

                showToast("You have joined the event!")

            } else {
                // Entrée payante
                if (party.paymentLink != null && party.paymentLink!!.isNotEmpty()) {
                    val intent = Intent(Intent.ACTION_VIEW, party.paymentLink!!.toUri())
                    startActivity(intent)
                } else {
                    showToast("Payment link is empty.")
                }
            }
        } else {
            showToast("All tickets are already taken!")
        }
    }
    private fun getUserEmail(): String? {
        val sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        // Obtenez l'adresse e-mail enregistrée, si elle existe
        return sharedPreferences.getString("userEmail", null)
    }
}