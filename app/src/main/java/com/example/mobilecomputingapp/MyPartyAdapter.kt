import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilecomputingapp.JoinedActivity
import com.example.mobilecomputingapp.ModifyPartyForm
import com.example.mobilecomputingapp.Party
import com.example.mobilecomputingapp.R
import com.google.firebase.database.FirebaseDatabase

class MyPartyAdapter(private val context: Context, var myPartyList: List<Party>) :
    RecyclerView.Adapter<MyPartyAdapter.PartyViewHolder>() {

    inner class PartyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val partyName: TextView = itemView.findViewById(R.id.partyName)
        val partyAddress: TextView = itemView.findViewById(R.id.partyAddress)
        val partyDate: TextView = itemView.findViewById(R.id.partyDate)
        val partyInfo: Button = itemView.findViewById(R.id.mypartyInfo)
        val buttonModify: Button = itemView.findViewById(R.id.buttonModify)
        val buttonDelete: Button = itemView.findViewById(R.id.buttonDelete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PartyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.my_party_item, parent, false)
        return PartyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PartyViewHolder, position: Int) {
        val currentParty = myPartyList[position]

        holder.partyName.text = currentParty.name
        holder.partyAddress.text = currentParty.address
        holder.partyDate.text = currentParty.date

        holder.partyInfo.setOnClickListener {
            when (context) {
                is JoinedActivity -> {
                    val joinedActivity = context as JoinedActivity
                    joinedActivity.showMyPartyInfoDialog(currentParty)
                }
                // Add more cases for other activity contexts if needed
                else -> {
                    Toast.makeText(
                        context,
                        "Problème d'annonce chez my party adapter",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        // Add similar onClickListener for buttonModify and buttonDelete if needed
        holder.buttonModify.setOnClickListener {
            // Ouvrez le formulaire de modification et passez les informations de la fête
            val intent = Intent(context, ModifyPartyForm::class.java)
            intent.putExtra("partyToModify", currentParty)
            context.startActivity(intent)
        }

        holder.buttonDelete.setOnClickListener {
            removeParty(currentParty)
        }
    }

    override fun getItemCount(): Int {
        return myPartyList.size
    }

    fun removeParty(party: Party) {
        // Supprimer la fête de la base de données en utilisant son ID
        party.id?.let { partyId ->
            val databaseReferenceParties = FirebaseDatabase.getInstance().reference.child("parties")
            databaseReferenceParties.child(partyId).removeValue()
                .addOnSuccessListener {
                    // Mettez à jour l'adaptateur après la suppression
                    myPartyList = myPartyList.filterNot { it.id == partyId }
                    notifyDataSetChanged()
                    Toast.makeText(context, "Party deleted successfully", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(context, "Failed to delete party", Toast.LENGTH_SHORT).show()
                }
        }
    }

    // Fonction pour mettre à jour les données de l'adaptateur
    fun updateData(newPartyList: List<Party>) {
        myPartyList = newPartyList
        notifyDataSetChanged()
    }
}