package com.example.mobilecomputingapp

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.widget.EditText
import android.widget.Toast
import android.widget.Toast.*
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.mobilecomputingapp.databinding.ActivityModifyPartyFormBinding
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.util.Arrays
import java.util.Calendar
import java.util.Locale

class ModifyPartyForm : AppCompatActivity() {

    private lateinit var binding: ActivityModifyPartyFormBinding
    private var placesClient: PlacesClient? = null
    private lateinit var database: DatabaseReference

    private var partyLatitude: Double = 0.0
    private var partyLongitude: Double = 0.0


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, getString(R.string.api_key), Locale.UK)
        }

        super.onCreate(savedInstanceState)

        binding = ActivityModifyPartyFormBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())

        database = FirebaseDatabase.getInstance().reference

        placesClient = Places.createClient(this)

        val partyToModify: Party? = intent.getSerializableExtra("partyToModify") as? Party

        binding.address.setText(partyToModify?.address)
        binding.partyname.setText(partyToModify?.name)
        binding.dateparty.setText(partyToModify?.date)
        binding.partynumber.setText(partyToModify?.numberMax.toString())
        binding.partyprice.setText(partyToModify?.price.toString())
        binding.partyplink.setText(partyToModify?.paymentLink)
        binding.partyfrom.setText(partyToModify?.from)
        binding.partyto.setText(partyToModify?.to)
        binding.partydescription.setText(partyToModify?.description)

        binding.address.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                startAutocompleteIntent()
            }
        }

        binding.address.setOnClickListener {
            startAutocompleteIntent()
        }

        val datePartyEditText: EditText = findViewById(R.id.dateparty)
        datePartyEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                showDatePickerDialog()
            }
            true
        }

        val partyFromEditText: EditText = findViewById(R.id.partyfrom)
        partyFromEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                showTimePickerDialogFrom()
            }
            true
        }

        val partyToEditText: EditText = findViewById(R.id.partyto)
        partyToEditText.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                showTimePickerDialogTo()
            }
            true
        }

        binding.createButton.setOnClickListener {
            savePartyToFirebase()
            onBackPressed()
        }

        binding.addpartyformback.setOnClickListener{
            onBackPressed()
        }
    }

    private val startAutocomplete = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val intent = result.data
            if (intent != null) {
                val place = Autocomplete.getPlaceFromIntent(intent)
                fillInAddress(place)
            }
        } else if (result.resultCode == RESULT_CANCELED) {
            Log.i("tag", "User canceled autocomplete")
        }
    }

    private fun startAutocompleteIntent() {
        val fields = Arrays.asList(
            Place.Field.ADDRESS_COMPONENTS,
            Place.Field.LAT_LNG, Place.Field.VIEWPORT
        )

        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
            .setTypesFilter(object : ArrayList<String?>() {
                init {
                    add(TypeFilter.ADDRESS.toString().lowercase(Locale.getDefault()))
                }
            })
            .build(this)

        startAutocomplete.launch(intent)
    }

    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(this, { _, selectedYear, selectedMonth, selectedDay ->
            val selectedDate = "$selectedDay/${selectedMonth + 1}/$selectedYear"
            val datePartyEditText: EditText = findViewById(R.id.dateparty)
            datePartyEditText.setText(selectedDate)
        }, year, month, day)

        datePickerDialog.show()
    }

    private fun showTimePickerDialogFrom() {
        val calendar = Calendar.getInstance()
        val currentHour = calendar.get(Calendar.HOUR_OF_DAY)
        val currentMinute = calendar.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(
            this,
            { _, hourOfDay, minute ->
                // Traitement de l'heure sélectionnée
                val selectedTime = String.format("%02d:%02d", hourOfDay, minute)
                val partyFromEditText: EditText = findViewById(R.id.partyfrom)
                partyFromEditText.setText(selectedTime)
            },
            currentHour,
            currentMinute,
            true // Afficher le mode 24 heures
        )

        timePickerDialog.show()
    }

    private fun showTimePickerDialogTo() {
        val calendar = Calendar.getInstance()
        val currentHour = calendar.get(Calendar.HOUR_OF_DAY)
        val currentMinute = calendar.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(
            this,
            { _, hourOfDay, minute ->
                // Traitement de l'heure sélectionnée
                val selectedTime = String.format("%02d:%02d", hourOfDay, minute)
                val partyToEditText: EditText = findViewById(R.id.partyto)
                partyToEditText.setText(selectedTime)
            },
            currentHour,
            currentMinute,
            true // Afficher le mode 24 heures
        )

        timePickerDialog.show()
    }

    private fun fillInAddress(place: Place) {
        val components = place.addressComponents
        val address = StringBuilder()

        if (components != null) {
            for (component in components.asList()) {
                val type = component.types[0]
                when (type) {
                    "street_number" -> {
                        address.insert(0, component.name)
                    }

                    "route" -> {
                        address.append(" ")
                        address.append(component.name)
                    }

                    "postal_code" -> {
                        address.append(" - ").append(component.name)
                    }

                    "postal_code_suffix" -> {
                        address.append("-").append(component.name)
                    }

                    "locality" -> address.append(" - ").append(component.name)
                    "administrative_area_level_1" -> {
                        address.append(", ").append(component.name)
                    }

                    "country" -> address.append(", ").append(component.name)
                }
            }
        }

        partyLatitude = place.latLng?.latitude ?: 0.0
        partyLongitude = place.latLng?.longitude ?: 0.0

        Log.d("Longitude", partyLongitude.toString())
        Log.d("Latitude", partyLatitude.toString())

        binding.address.setText(address.toString())
    }

    private fun getUserEmail(): String? {
        val sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        // Obtenez l'adresse e-mail enregistrée, si elle existe
        return sharedPreferences.getString("userEmail", null)
    }

    private fun savePartyToFirebase() {
        val partyToModify: Party? = intent.getSerializableExtra("partyToModify") as? Party

        // Assurez-vous que la fête à modifier existe
        if (partyToModify != null) {
            // Stockez la valeur initiale de paymentLink
            val originalPaymentLink = partyToModify.paymentLink

            // Mettez à jour les propriétés de la fête existante avec les nouvelles valeurs
            partyToModify.name = binding.partyname.text.toString()
            partyToModify.address = binding.address.text.toString()
            partyToModify.date = binding.dateparty.text.toString()
            partyToModify.from = binding.partyfrom.text.toString()
            partyToModify.to = binding.partyto.text.toString()
            partyToModify.description = binding.partydescription.text.toString()
            partyToModify.latitude = partyLatitude
            partyToModify.longitude = partyLongitude
            partyToModify.partyOwner = getUserEmail()
            partyToModify.numberMax = binding.partynumber.text.toString().toInt()
            partyToModify.price = binding.partyprice.text.toString().toDouble()
            partyToModify.paymentLink = binding.partyplink.text.toString()

            // Mettez à jour la fête dans la base de données en utilisant son ID
            val partyId = partyToModify.id
            if (partyId != null) {
                database.child("parties").child(partyId).setValue(partyToModify)

                // Restaurez la valeur initiale de paymentLink après la modification
                partyToModify.paymentLink = originalPaymentLink
                Toast.makeText(this, "Event Modified !", LENGTH_SHORT).show()
            }
        }
    }


}

